### Cynthia CMS  
[![license](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat)](https://gitlab.com/huaiping/cynthia/blob/master/LICENSE) [![Build Status](https://travis-ci.com/huaiping/cynthia.svg?branch=master)](https://travis-ci.com/huaiping/cynthia) [![Coverage Status](https://coveralls.io/repos/github/huaiping/cynthia/badge.svg?branch=master)](https://coveralls.io/github/huaiping/cynthia?branch=master)  
Open Source CMS. Just for practice.

### Requirements
Ruby 2.3+ [https://www.ruby-lang.org](https://www.ruby-lang.org)  
MySQL 5.5+ [https://www.mysql.com](https://www.mysql.com)

### Features
HTML 5 + Rails 5.2.2 + Bootstrap 3.4.0

### Installation
```
$ git clone https://github.com/huaiping/cynthia.git
$ cd cynthia
$ bundle install
$ rails server
```

### Demo
[https://www.huaiping.net](https://huaiping.net/v2/)

### License
Licensed under the [MIT License](https://gitlab.com/huaiping/cynthia/blob/master/LICENSE).
