class GuestbookController < ApplicationController
  def index
    @messages = Guestbook.all
  end

  def message
    redirect_to :controller => "membership", :action => "index"
  end
end
