class CreateGuestbooks < ActiveRecord::Migration[5.2]
  def change
    create_table :swan_comment do |t|
      t.string :sid
      t.string :content
      t.string :author
      t.string :avatar
      t.datetime :pubtime
      t.string :status
    end

    create_table :swan_course do |t|
      t.string :xh
      t.string :xm
      t.string :wl
      t.string :hx
      t.string :sw
      t.string :zz
      t.string :ls
      t.string :dl
      t.string :courses
      t.datetime :timestamp
      t.string :memo
      t.string :status
    end

    create_table :swan_download do |t|
      t.string :title
      t.string :category
      t.string :description
      t.string :url
      t.string :author
      t.datetime :pubtime
      t.string :sizes
      t.string :license
      t.string :stars
      t.integer :hits
      t.string :status
    end

    create_table :swan_exam do |t|
      t.string :question
      t.string :options
      t.string :answer
      t.string :pattern
      t.string :status
    end

    create_table :swan_guestbook do |t|
      t.string :title
      t.string :content
      t.string :author
      t.string :avatar
      t.datetime :pubtime
      t.string :reply
      t.datetime :modtime
      t.string :status
      t.timestamps
    end

    create_table :swan_links do |t|
      t.string :title
      t.string :url
      t.string :description
      t.string :author
      t.datetime :pubtime
      t.integer :hits
      t.string :status
    end

    create_table :swan_membership do |t|
      t.string :username
      t.string :password
      t.string :name
      t.string :email
      t.string :avatar
      t.string :groups
      t.string :articles
      t.string :albums
      t.string :files
      t.string :messages
      t.string :videos
      t.string :links
      t.string :survey
      t.string :exams
      t.string :salary
      t.string :repairs
      t.string :course
      t.datetime :pubtime
      t.integer :beans
      t.string :status
      t.string :memo
    end

    create_table :swan_news do |t|
      t.string :title
      t.string :summary
      t.text :content
      t.string :author
      t.string :category
      t.integer :hits
      t.string :status
    end

    create_table :swan_photo do |t|
      t.string :title
      t.string :description
      t.string :url
      t.string :thumbnail
      t.string :author
      t.string :category
      t.string :subcategory
      t.datetime :pubtime
      t.integer :hits
      t.string :status
    end

    create_table :swan_repairs do |t|
      t.string :category
      t.string :department
      t.string :item
      t.string :adress
      t.string :details
      t.string :customer
      t.string :telephone
      t.datetime :pubtime
      t.string :status
      t.string :assessment
    end

    create_table :swan_score do |t|
      t.string :xh
      t.string :xm
    end

    create_table :swan_salary do |t|
      t.string :bh
      t.string :xm
    end

    create_table :swan_survey do |t|
      t.string :xh
      t.string :xm
      t.string :yw
      t.string :sx
      t.string :yy
      t.string :wl
      t.string :hx
      t.string :sw
      t.string :zz
      t.string :ls
      t.string :dl
      t.string :message
      t.string :memo
    end

    create_table :swan_video do |t|
      t.string :title
      t.string :description
      t.string :url
      t.string :author
      t.string :category
      t.string :status
    end

    create_table :sessions do |t|
      t.string :session_id, :null => false
      t.text :data
      t.timestamps
    end

    add_index :sessions, :session_id, :unique => true
    add_index :sessions, :updated_at
  end
end
