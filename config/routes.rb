Rails.application.routes.draw do
  get 'course/index'
  get 'membership/index'
  get 'download/index'
  get 'video/index'
  get 'survey/index'
  get 'score/index'
  get 'salary/index'
  get 'photo/index'
  get 'news/index'
  get 'exam/index'
  get 'repairs/index'
  get 'guestbook/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
